#### import libraries ########
from flask import Flask,render_template, request
import sqlite3
from sqlite3 import Error
from contextlib import closing
from os import path

home = path.expanduser('~')



app = Flask(__name__)

### create and connect with database ####
def get_db():
    try:
        return sqlite3.connect(path.join(home,'testdatabase.db'))

    except Error:
        return Error


@app.route('/result',methods=['GET'])
def fetch_data():
    if request.method == 'GET':
        try:
            with closing(get_db()) as conn:
                cur = conn.cursor()
                data=cur.execute('SELECT * from TEST')
                render_data=data.fetchall()
                capture_log(request.url,request.method)
                return render_template('view.html',render_data=render_data)
        except Exception as err:
            return render_template('view.html',error=err)



def capture_log(*params):
    with closing(get_db()) as conn:
        cur = conn.cursor()
        insertquery = "insert into log(url,request_type,created) values(?,?,datetime('now')) "

        cur.execute(insertquery, params)
        conn.commit()



if __name__ == '__main__':
    app.run(debug=True)
